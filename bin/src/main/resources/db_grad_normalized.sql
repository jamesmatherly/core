
-- -----------------------------------------------------
-- Schema db_grad_normalized
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `db_grad_normalized` ;

-- -----------------------------------------------------
-- Schema db_grad_normalized
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `db_grad_normalized` DEFAULT CHARACTER SET utf8 ;
USE `db_grad_normalized` ;

-- -----------------------------------------------------
-- Table `instrument`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `instrument` ;

CREATE TABLE IF NOT EXISTS `instrument` (
  `instrument_id` INT AUTO_INCREMENT,
  `instrument_name` VARCHAR(35) NOT NULL,
  PRIMARY KEY (`instrument_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `counterparty`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `counterparty` ;

CREATE TABLE IF NOT EXISTS `counterparty` (
  `counterparty_id` INT(11) AUTO_INCREMENT,
  `counterparty_name` CHAR(30) NOT NULL,
  `counterparty_status` CHAR(1) NULL DEFAULT NULL,
  `counterparty_date_registered` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`counterparty_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `deal`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deal` ;

CREATE TABLE IF NOT EXISTS `deal` (
  `deal_id` INT(11) NOT NULL,
  `deal_time` VARCHAR(30) NOT NULL,
  `deal_type` CHAR(1) NULL DEFAULT NULL,
  `deal_amount` DECIMAL(12,2) NULL DEFAULT NULL,
  `deal_quantity` INT(11) NOT NULL,
  `deal_instrument_id` INT NOT NULL,
  `deal_counterparty_id` INT(11) NOT NULL,
  PRIMARY KEY (`deal_id`),
  CONSTRAINT `fk_deal_instrument0`
    FOREIGN KEY (`deal_instrument_id`)
    REFERENCES `instrument` (`instrument_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_deal_copy1_counterparty1`
    FOREIGN KEY (`deal_counterparty_id`)
    REFERENCES `counterparty` (`counterparty_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `users` ;

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` VARCHAR(60) NOT NULL,
  `user_pwd` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`user_id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Data population
-- -----------------------------------------------------

INSERT INTO `users` VALUES ('alison','gradprog2016@07'),('debs','gradprog2016@02'),('estelle','gradprog2016@05'),('john','gradprog2016@03'),('pauline','gradprog2016@04'),('samuel','gradprog2016@06'),('selvyn','gradprog2016');

INSERT INTO db_grad_normalized.counterparty ( counterparty_name, counterparty_status, counterparty_date_registered ) SELECT DISTINCT counterparty_name, counterparty_status, counterparty_date_registered FROM db_grad_cs_1917.deal;

INSERT INTO db_grad_normalized.instrument ( instrument_name ) SELECT DISTINCT instrument_name FROM db_grad_cs_1917.deal;

INSERT INTO db_grad_normalized.deal ( deal_id, deal_time, deal_counterparty_id, deal_instrument_id, deal_type, deal_amount, deal_quantity ) SELECT d.deal_id, STR_TO_DATE(d.deal_time, "%Y-%m-%dT0%h:%i:%s.%f"), c.counterparty_id, i.instrument_id, d.deal_type, d.deal_amount, d.deal_quantity FROM db_grad_cs_1917.deal d, db_grad_normalized.counterparty c, db_grad_normalized.instrument i WHERE d.counterparty_name = c.counterparty_name AND d.instrument_name = i.instrument_name;
