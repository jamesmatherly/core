/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.database;

import java.util.Properties;

/**
 *
 * @author Selvyn
 */
public class ApplicationScopeHelper
{

    private String itsInfo = "NOT SET";
    private DBConnector itsConnector = null;

    public String getInfo()
    {
        return itsInfo;
    }

    public void setInfo(String itsInfo)
    {
        this.itsInfo = itsInfo;
    }

    public boolean bootstrapDBConnection()
    {
        boolean result = false;
        if (itsConnector == null)
        {
            itsConnector = DBConnector.getConnector();

            PropertyLoader pLoader = PropertyLoader.getLoader();

            Properties pp;
            pp = pLoader.getPropValues("dbConnector.properties");

            result = itsConnector.connect(pp);
        }
        else
            result = true;
        
        return result;
    }

}
