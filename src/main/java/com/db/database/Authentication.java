package com.db.database;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Authentication {
	private String hash;
    private String salt;
    private String password;
    private String hashedPassword;
    private String retrievedPassword;
    private String username;
    private static DBConnector conn;
    private boolean isUser;
    private boolean isAuthenticated;
    private ResultSet users;

    public Authentication(String username, String password){
        this.password = password;
        this.username = username;
        setHashedPassword();
        conn = DBConnector.getConnector();
        try {
            doesUserExist();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(this.isUser) {
            authenicate();
        }
    }

    protected boolean getIsUser() {
        return isUser;
    }

    protected void doesUserExist() throws SQLException {
        /*
        * iterate through user table until matching username is found
        */
        this.users = conn.query("select * from users");
        this.isUser = false;
        while(this.users.next()) {
            if (this.username.equals(this.users.getString(1))) {
                this.getUserDetails();
                this.isUser = true;
                break;
            }
        }
    }

    protected void authenicate() {
        if(this.retrievedPassword.equals(this.hashedPassword)){
            this.isAuthenticated = true;
        }
        else {
            this.isAuthenticated = false;
        }
    }

	protected void getUserDetails(){
        try {
            this.retrievedPassword = this.users.getString(2);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    protected void setHashedPassword(){
        /*
        * this should be in charge of hashing password
        * hashing not yet implemented
         */
        this.hashedPassword = this.password;
    }

    public String getRetrievedPassword() {
        return retrievedPassword;
    }

    public String getUsername() {
        return username;
    }

    public boolean getIsAuthenticated() {
        return isAuthenticated;
    }

}
