package com.db.database;

import com.db.entity.Counterparty;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class CounterpartyDao {
    private static final String QUERY_GET_ALL_COUNTERPARTY = "SELECT counterparty_id, counterparty_name, counterparty_status, counterparty_date_registered FROM counterparty";
    private static DBConnector conn = DBConnector.getConnector();


    public List<Counterparty> getAllCounterpartys() throws SQLException {
        return getCounterpartyList(QUERY_GET_ALL_COUNTERPARTY);
    }

    private List<Counterparty> getCounterpartyList(String query) throws SQLException {
        ResultSet resultSet = conn.query(query);
        List<Counterparty> instrumentList = new LinkedList<>();
        while (resultSet.next()) {
            instrumentList.add(new Counterparty(
                    resultSet.getLong(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4)
                    ));
        }
        return instrumentList;
    }

    public Counterparty getCounterpartyById(Long deal_id) throws SQLException {
        String query = QUERY_GET_ALL_COUNTERPARTY + " WHERE counterparty_id = " + deal_id;
        ResultSet result = conn.query(query);
        if (result.next()) {
            return new Counterparty(
                    result.getLong(1),
                    result.getString(2),
                    result.getString(3),
                    result.getString(4)
            );
        } else {
            return null;
        }
    }
}
