package com.db.entity;

public class AverageBuyAndSell {
    private String instrumentName;
    private double averageBuy;
    private double averageSell;

    public AverageBuyAndSell(String instrumentName, double averageBuy, double averageSell) {
        this.instrumentName = instrumentName;
        this.averageBuy = averageBuy;
        this.averageSell = averageSell;
    }

    public double getAverageBuy() {
        return this.averageBuy;
    }

    public double getAverageSell() {
        return this.averageSell;
    }

    public String getInstrumentName() {
        return this.instrumentName;
    }
}

