package com.db.entity;

public class Counterparty {
    private long counterparty_id;
    private String counterparty_name;
    private String counterparty_status;
    private String counterparty_date_registered;

    public Counterparty(long counterparty_id,
                        String counterparty_name,
                        String counterparty_status,
                        String counterparty_date_registered) {
        this.counterparty_id = counterparty_id;
        this.counterparty_name = counterparty_name;
        this.counterparty_status = counterparty_status;
        this.counterparty_date_registered = counterparty_date_registered;
    }

    public Counterparty() {
    }

    public long getCounterparty_id() {
        return counterparty_id;
    }

    public void setCounterparty_id(long counterparty_id) {
        this.counterparty_id = counterparty_id;
    }

    public void setCounterparty_name(String counterparty_name) {
        this.counterparty_name = counterparty_name;
    }

    public String getCounterparty_status() {
        return counterparty_status;
    }

    public void setCounterparty_status(String counterparty_status) {
        this.counterparty_status = counterparty_status;
    }

    public String getCounterparty_date_registered() {
        return counterparty_date_registered;
    }

    public void setCounterparty_date_registered(String counterparty_date_registered) {
        this.counterparty_date_registered = counterparty_date_registered;
    }

}
