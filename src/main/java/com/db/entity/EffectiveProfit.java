package com.db.entity;

public class EffectiveProfit {
    private String counterpartyName;
    private String instrumentName;
    private double effectiveProfit;

    public EffectiveProfit(String counterpartyName, String instrumentName, double effectiveProfit) {
        this.counterpartyName = counterpartyName;
        this.instrumentName = instrumentName;
        this.effectiveProfit = effectiveProfit;
    }

    public String getCounterpartyName() {
        return this.counterpartyName;
    }

    public String getInstrumentName() {
        return this.instrumentName;
    }

    public double getEffectiveProfit() {
        return this.effectiveProfit;
    }
}
