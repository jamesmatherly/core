package com.db.entity;

public class RealizedProfit {
    private String counterpartyName;
    private String instrumentName;
    private double realizedProfit;

    public RealizedProfit(String counterpartyName, String instrumentName, double realizedProfit) {
        this.counterpartyName = counterpartyName;
        this.instrumentName = instrumentName;
        this.realizedProfit = realizedProfit;
    }

    public String getCounterpartyName() {
        return this.counterpartyName;
    }

    public String getInstrumentName() {
        return this.instrumentName;
    }

    public double getRealizedProfit() {
        return this.realizedProfit;
    }
}
