package com.db.service;

import com.db.database.*;
import com.db.entity.*;
import com.db.util.ResourceReader;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;


public class DBService {
    private static final String AVERAGE_BUY_AND_SELL = "SELECT\n" +
            "    instrument_name,\n" +
            "    AVG(CASE\n" +
            "        WHEN deal_type = 'B' THEN deal_amount\n" +
            "    END) AS average_buy_price,\n" +
            "    AVG(CASE\n" +
            "        WHEN deal_type = 'S' THEN deal_amount\n" +
            "    END) AS average_sell_price\n" +
            "FROM\n" +
            "    deal\n" +
            "        JOIN\n" +
            "    counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id\n" +
            "        JOIN\n" +
            "    instrument ON instrument.instrument_id = deal.deal_instrument_id\n" +
            "GROUP BY deal_instrument_id";

    private static final String ENDING_POSITION = "SELECT\n" +
            "    t.counterparty_name,\n" +
            "    t.instrument_name,\n" +
            "    buy - sell AS ending_position\n" +
            "FROM\n" +
            "    (SELECT\n" +
            "            counterparty.counterparty_name,\n" +
            "            instrument.instrument_name,\n" +
            "            SUM(CASE\n" +
            "                WHEN deal_type = 'B' THEN deal_quantity\n" +
            "            END) AS buy,\n" +
            "            SUM(CASE\n" +
            "                WHEN deal_type = 'S' THEN deal_quantity\n" +
            "            END) AS sell\n" +
            "    FROM\n" +
            "        deal\n" +
            "    JOIN counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id\n" +
            "    JOIN instrument ON instrument.instrument_id = deal.deal_instrument_id\n" +
            "    GROUP BY deal_counterparty_id , deal_instrument_id) AS t";

    private static final String REALIZED_PROFIT = "SELECT\n" +
            "    t.counterparty_name,\n" +
            "    t.instrument_name,\n" +
            "    ((sell_amount / total_sell_quantity) - (buy_amount / total_buy_quantity)) * total_sell_quantity AS realized_profit\n" +
            "FROM\n" +
            "    (SELECT\n" +
            "            counterparty.counterparty_name,\n" +
            "            instrument.instrument_name,\n" +
            "            deal_counterparty_id,\n" +
            "            deal_instrument_id,\n" +
            "            SUM(CASE\n" +
            "                WHEN deal_type = 'B' THEN deal_quantity\n" +
            "            END) AS total_buy_quantity,\n" +
            "            SUM(CASE\n" +
            "                WHEN deal_type = 'S' THEN deal_quantity\n" +
            "            END) AS total_sell_quantity,\n" +
            "            SUM(CASE\n" +
            "                WHEN deal_type = 'B' THEN deal_amount * deal_quantity\n" +
            "            END) AS buy_amount,\n" +
            "            SUM(CASE\n" +
            "                WHEN deal_type = 'S' THEN deal_amount * deal_quantity\n" +
            "            END) AS sell_amount\n" +
            "    FROM\n" +
            "        deal\n" +
            "    JOIN counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id\n" +
            "    JOIN instrument ON instrument.instrument_id = deal.deal_instrument_id\n" +
            "    GROUP BY deal_counterparty_id , deal_instrument_id) AS t";

    private static DBConnector conn = DBConnector.getConnector();

    public static boolean validateUser(String login, String password) throws SQLException {
        return new UsersDao().checkUser(login, password);
    }

    public static Deal getDealById(Long deal_id) throws SQLException {
        return new DealDao().getDealById(deal_id);
    }

    public static List<Deal> getAllDeals() throws SQLException {
        return new DealDao().getAllDeals();
    }

    public static List<Instrument> getAllInstruments() throws SQLException {
        return new InstrumentDao().getAllInstruments();
    }

    public static List<Counterparty> getCounterparty() throws SQLException {
        return new CounterpartyDao().getAllCounterpartys();
    }

    public static Instrument getInstrumentByID(long instrument_id) throws SQLException {
        return new InstrumentDao().getInstrumentById(instrument_id);
    }

    public static List<Deal> getDealsPage(int pageNumber, int pageSize) throws SQLException {
        return new DealDao().getDealsPage(pageNumber, pageSize);
    }

    public static List<Deal> getDealsPage(int pageNumber) throws SQLException {
        return getDealsPage(pageNumber, 1000);
    }


    public static List<AverageBuyAndSell> getAverageBuyAndSell() throws SQLException {
        List<AverageBuyAndSell> result = new LinkedList<>();
        ResultSet resultSet = conn.query(AVERAGE_BUY_AND_SELL);
        while (resultSet.next()) {
            result.add(new AverageBuyAndSell(
                    resultSet.getString(1),
                    resultSet.getDouble(2),
                    resultSet.getDouble(3)));
        }
        return result;
    }

    public static List<EndingPosition> getEndingPosition() throws SQLException {
        List<EndingPosition> result = new LinkedList<>();
        ResultSet resultSet = conn.query(ENDING_POSITION);
        while (resultSet.next()) {
            result.add(new EndingPosition(
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getLong(3)));
        }
        return result;
    }

    public static List<RealizedProfit> getRealizedProfit() throws SQLException {
        List<RealizedProfit> result = new LinkedList<>();
        ResultSet resultSet = conn.query(REALIZED_PROFIT);
        while (resultSet.next()) {
            result.add(new RealizedProfit(
                    resultSet.getString(1),
                    resultSet.getString(2),
                    resultSet.getDouble(3)));
        }
        return result;
    }

//    public static List<EffectiveProfit> getEffectiveProfit()  throws SQLException {
//        List<EffectiveProfit> result =  new LinkedList<>();
//        try {
//            String query = ResourceReader.getQuery("effectiveProfitOrLoss.sql");
//            ResultSet resultSet = conn.query(query);
//            while (resultSet.next()) {
//                result.add(new EffectiveProfit(
//                        resultSet.getString(1),
//                        resultSet.getString(2),
//                        resultSet.getDouble(3)));
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return result;
//    }

    public static List<EffectiveProfit> getEffectiveProfit() throws SQLException {
        List<EffectiveProfit> effectiveProfitList = new LinkedList<>();
        String EffectiveProfitListSelect = "SELECT \n" +
                "    counterparty_name,\n" +
                "    instrument_name,\n" +
                "    deal_counterparty_id,\n" +
                "    deal_instrument_id,\n" +
                "    ((sell_amount / total_sell_quantity) - (buy_amount / total_buy_quantity)) * total_sell_quantity AS realized_profit,\n" +
                "    (total_buy_quantity - total_sell_quantity) AS difference_quantity\n" +
                "FROM\n" +
                "    (SELECT \n" +
                "        counterparty_name,\n" +
                "            instrument_name,\n" +
                "            deal_instrument_id,\n" +
                "            deal_counterparty_id,\n" +
                "            SUM(CASE\n" +
                "                WHEN deal_type = 'B' THEN deal_quantity\n" +
                "            END) AS total_buy_quantity,\n" +
                "            SUM(CASE\n" +
                "                WHEN deal_type = 'S' THEN deal_quantity\n" +
                "            END) AS total_sell_quantity,\n" +
                "            SUM(CASE\n" +
                "                WHEN deal_type = 'B' THEN deal_amount * deal_quantity\n" +
                "            END) AS buy_amount,\n" +
                "            SUM(CASE\n" +
                "                WHEN deal_type = 'S' THEN deal_amount * deal_quantity\n" +
                "            END) AS sell_amount\n" +
                "    FROM\n" +
                "        deal\n" +
                "    JOIN counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id\n" +
                "    JOIN instrument ON instrument.instrument_id = deal.deal_instrument_id\n" +
                "    GROUP BY deal_counterparty_id , deal_instrument_id) AS t";

        List<InstrumentCost> times = new LinkedList<>();
        String timeSelect = "SELECT \n" +
                "    deal_instrument_id,\n" +
                "    MAX(CASE\n" +
                "        WHEN deal_type = 'B' THEN deal_time\n" +
                "    END) AS end_buy_time,\n" +
                "    MAX(CASE\n" +
                "        WHEN deal_type = 'S' THEN deal_time\n" +
                "    END) AS end_sell_time\n" +
                "FROM\n" +
                "    deal\n" +
                "GROUP BY deal_instrument_id";
        ResultSet resultSetTimes = conn.query(timeSelect);
        while (resultSetTimes.next()) {
            long deal_instrument_id = resultSetTimes.getLong(1);
            String end_buy_time = resultSetTimes.getString(2);
            String end_sell_time = resultSetTimes.getString(3);
            times.add(new InstrumentCost(deal_instrument_id, end_buy_time, end_sell_time));
        }

        ResultSet resultSetEffectiveProfitList = conn.query(EffectiveProfitListSelect);
        while (resultSetEffectiveProfitList.next()) {
            String counterpartyName = resultSetEffectiveProfitList.getString(1);
            long deal_instrument_id = resultSetEffectiveProfitList.getLong(4);
            String instrumentName = resultSetEffectiveProfitList.getString(2);
            double realizedProfit = resultSetEffectiveProfitList.getDouble(5);
            long endPosition = resultSetEffectiveProfitList.getLong(6);

            String latest_buy_time = "";
            String latest_sell_time = "";
            for (InstrumentCost l : times) {
                if (l.getDeal_instrument_id() == deal_instrument_id) {
                    latest_buy_time = l.getEnd_buy_time();
                    latest_sell_time = l.getEnd_sell_time();
                    break;
                }
            }

            double effectivePrice;
            if (endPosition < 0) {
                effectivePrice = getEffectivePrice(deal_instrument_id, realizedProfit, endPosition, latest_sell_time);
            } else {
                effectivePrice = getEffectivePrice(deal_instrument_id, realizedProfit, endPosition, latest_buy_time);
            }
            effectiveProfitList.add(new EffectiveProfit(counterpartyName, instrumentName, effectivePrice));
        }
        return effectiveProfitList;
    }

    private static double getEffectivePrice(long dealInstrumentId, double realizedProfit, long endPosition, String latestBuyTime) throws SQLException {
        double effectivePrice;
        String amount = "SELECT \n" +
                "    deal_amount\n" +
                "FROM\n" +
                "    deal\n" +
                "WHERE\n" +
                "    deal_time = '" + latestBuyTime + "'\n" +
                "        AND deal_instrument_id = " + dealInstrumentId;
        ResultSet resultSetAmount = conn.query(amount);
        resultSetAmount.next();
        double buy = resultSetAmount.getDouble(1);
        double buyEndPos = buy * endPosition;
        effectivePrice = realizedProfit + buyEndPos;
        return effectivePrice;
    }
}