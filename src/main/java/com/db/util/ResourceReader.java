package com.db.util;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ResourceReader {

    public static void main(String[] args) {
    }

    public static String getQuery(String fileName) throws IOException {
        ClassLoader classLoader = new ResourceReader().getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        String content = new String(Files.readAllBytes(file.toPath()));
        return content;
    }
}