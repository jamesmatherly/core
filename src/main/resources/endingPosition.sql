SELECT
    t.counterparty_name,
    t.instrument_name,
    buy - sell AS ending_position
FROM
    (SELECT
            counterparty.counterparty_name,
            instrument.instrument_name,
            SUM(CASE
                WHEN deal_type = 'B' THEN deal_quantity
            END) AS buy,
            SUM(CASE
                WHEN deal_type = 'S' THEN deal_quantity
            END) AS sell
    FROM
        deal
    JOIN counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id
    JOIN instrument ON instrument.instrument_id = deal.deal_instrument_id
    GROUP BY deal_counterparty_id , deal_instrument_id) AS t;
