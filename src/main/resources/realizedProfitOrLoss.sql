SELECT
    t.counterparty_name,
    t.instrument_name,
    ((sell_amount / total_sell_quantity) - (buy_amount / total_buy_quantity)) * total_sell_quantity AS realized_profit
FROM
    (SELECT
            counterparty_name,
            instrument_name,
            deal_instrument_id,
            deal_counterparty_id,
            SUM(CASE
                WHEN deal_type = 'B' THEN deal_quantity
            END) AS total_buy_quantity,
            SUM(CASE
                WHEN deal_type = 'S' THEN deal_quantity
            END) AS total_sell_quantity,
            SUM(CASE
                WHEN deal_type = 'B' THEN deal_amount * deal_quantity
            END) AS buy_amount,
            SUM(CASE
                WHEN deal_type = 'S' THEN deal_amount * deal_quantity
            END) AS sell_amount
    FROM
        deal
    JOIN counterparty ON counterparty.counterparty_id = deal.deal_counterparty_id
    JOIN instrument ON instrument.instrument_id = deal.deal_instrument_id
    GROUP BY deal_counterparty_id , deal_instrument_id) AS t;