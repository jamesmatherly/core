package com.db.database;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.sql.ResultSet;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class DBConnectorTest {

    private static final int NUM_ROWS_DEAL = 2000;
    private DBConnector dbconn;
    ApplicationScopeHelper ash = new ApplicationScopeHelper();

    public DBConnectorTest() {
    }

    @Before
    public void setUp() {
        System.out.println(ash.bootstrapDBConnection());
        dbconn = DBConnector.getConnector();
    }

//    @Test
//    public void testIsConnected() throws Exception
//    {
//        assertTrue(dbconn.connect());
//    }

    @Test
    public void testQueryReturnsCorrectOutputInUsersTable() throws Exception {
        ResultSet results = dbconn.query("select * from users");
        String[] users_expected = {"alison",
                "debs",
                "estelle",
                "john",
                "pauline",
                "samuel",
                "selvyn"};
        String[] pass_expected = {"gradprog2016@07",
                "gradprog2016@02",
                "gradprog2016@05",
                "gradprog2016@03",
                "gradprog2016@04",
                "gradprog2016@06",
                "gradprog2016"};
        ArrayList<String> users = new ArrayList<String>();
        ArrayList<String> pass = new ArrayList<String>();
        while (results.next()) {
            users.add(results.getString(1));
            pass.add(results.getString(2));
        }
        assertArrayEquals(users_expected, users.toArray());
        assertArrayEquals(pass_expected, pass.toArray());
    }

    @Test
    @Ignore
    public void testQueryReturnsCorrectNumberOfRowsInDealTable() throws Exception {
        ResultSet results = dbconn.query("select * from deal");
        int count = 0;
        while (results.next()) {
            count++;
        }
        assertEquals(NUM_ROWS_DEAL, count);
    }

}
